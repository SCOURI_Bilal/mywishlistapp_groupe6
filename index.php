<?php

require_once __DIR__ . '/vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \wish\conf\Eloquent;
use \wish\control\ParticipantController;
use \wish\control\CreateurController;
use \wish\control\CompteController;
use \wish\control\IndexController;

$config = require_once __DIR__ . '/src/conf/settings.php';

$c = new \Slim\Container($config);

$app = new \Slim\app($c);

Eloquent::start(__DIR__ . '/src/conf/db.config.ini');

$app->post('/item/suppression/{id}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->supprimerItem($rq,$rs,$args);
} )->setName('itemSuppr');

$app->post('/item/edition/{id}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->editionItem($rq,$rs,$args);
} )->setName('itemEdition');

$app->get('/item/edition/{id}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->affEditionItem($rq,$rs,$args);
} )->setName('affItemEdition');

$app->get('/item/creat/{id}', function (Request $rq, Response $rs, array $args): Response{
$c = new CreateurController($this);
return $c->displayItemCrea($rq,$rs,$args);
} )->setName('itemCreat');

$app->get('/item/{id}', function (Request $rq, Response $rs, array $args): Response{
    $c = new ParticipantController($this);
    return $c->displayItem($rq,$rs,$args);
} )->setName('item');

$app->post('/item/{id}', function (Request $rq, Response $rs, array $args): Response{
    $c = new ParticipantController($this);
    return $c->reserverItem($rq,$rs,$args);
})->setName('reserverItem');

$app->post('/liste/edition/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->editionListe($rq,$rs,$args);
} )->setName('listeEdition');

$app->get('/liste/edition/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->affEditionListe($rq,$rs,$args);
} )->setName('affListeEdition');

$app->post('/liste/suppression/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->supprimerListe($rq,$rs,$args);
} )->setName('listeSuppr');

$app->get('/liste/createur/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->displayListeCreateur($rq,$rs,$args);
} )->setName('listeCreateur');

$app->post('/liste/{tokenPartage}', function (Request $rq, Response $rs, array $args): Response{
    $c = new ParticipantController($this);
    return $c->ajouterMessageListe($rq,$rs,$args);
} )->setName('ajoutMessageListe');

$app->get('/liste/{tokenPartage}', function (Request $rq, Response $rs, array $args): Response{
    $c = new ParticipantController($this);
    return $c->displayListe($rq,$rs,$args);
} )->setName('liste');

$app->post('/creation/list', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->creeList($rq,$rs,$args);
})->setName('creationList');

$app->get('/creation/list', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
       return $c->affCreeList($rq,$rs,$args);
})->setName('affCreationList');

$app->post('/ajouter/item/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->ajouterItemList($rq,$rs,$args);
})->setName('ajoutItem');

$app->get('/ajouter/item/{tokenModification}', function (Request $rq, Response $rs, array $args): Response{
    $c = new CreateurController($this);
    return $c->affAjouterItemList($rq,$rs,$args);
})->setName('affAjoutItem');

$app->post('/connexion', function (Request $rq, Response $rs, array $args): Response{
    $c = new CompteController($this);
    return $c->connexion($rq,$rs,$args);
})->setName('connexion');

$app->get('/connexion', function (Request $rq, Response $rs, array $args): Response{
    $c = new CompteController($this);
    return $c->affConnexion($rq,$rs,$args);
})->setName('affConnexion');

$app->post('/inscription', function (Request $rq, Response $rs, array $args): Response{
    $c = new CompteController($this);
    return $c->inscription($rq,$rs,$args);
})->setName('inscription');

$app->get('/inscription', function (Request $rq, Response $rs, array $args): Response{
    $c = new CompteController($this);
    return $c->affInscription($rq,$rs,$args);
})->setName('affInscription');

$app->get('/deconnexion', function (Request $rq, Response $rs, array $args): Response{
    $c = new CompteController($this);
    return $c->deconnexion($rq,$rs,$args);
})->setName('deconnexion');

$app->get('[/]', function (Request $rq, Response $rs, array $args): Response{
    $c = new IndexController($this);
    return $c->affIndex($rq,$rs,$args);
})->setName('index');

/*
 * ne fonctionne pas pr l'instant
$app->get('/visualiser', function (Request $rq, Response $rs, array $args): Response{
    $c = new IndexController($this);
    return $c->visua($rq,$rs,$args);
})->setName('visualiser');
*/

$app->run();