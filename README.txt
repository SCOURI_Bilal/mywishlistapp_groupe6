utilisateurs : 
login : User Test mdp : MdpTest2021
login : User_Test$ mdp: MdpTest2020


Comment installer le projet sur votre machine ?

installer Xampp :
Aller sur le site https://www.apachefriends.org/fr/index.html et télécharger xampp, puis l’installer (si c’est sur Windows). Lors du lancement du control panel de xampp, accéder au phpmyadmin. 


installer PhpStorm :
aller sur le site https://www.jetbrains.com/ et installer phpstorm sur votre ordinateur.  
Après l’installation de phpstorm, créer un projet dans le répertoire \xampp\htdocs où sera situé le projet du git téléchargé depuis le bitbucket.
git clone (dans htdocs):
Cliquer sur le lien ci-dessous pour pouvoir accéder à notre dossier bitbucket et voir tout notre code:
https://bitbucket.org/SCOURI_Bilal/mywishlistapp_groupe6/src/master/
création de la base de donnée (phpmyadmin) :
Pour créer la base de donnée, accéder au “xampp control” installé précédemment, puis démarrer l’apache et le Mysql comme ceci : 
puis appuyer sur “Admin” sur la ligne de MYSQL. Cela va automatiquement générer une page phpmyadmin où l’on va pouvoir créer notre base de données. Par la suite, dans phpmyadmin cliquer sur  pour créer une base de données. Une page comme ceci va vous être affichée : 

Dans le ,entrer comme nom de base de données: mywishlist . Créer cette base de données. 
Pour importer les données de cette base, aller sur “importer”
puis faire “choisir un fichier”. Choisir pour celui-ci le fichier “mywishlist.sql” que l’on vous mettra dans le dépôt git, puis exécuter en bas de la page. Ceci générera la base de données avec ses tables et aussi les insertions effectué et prêt à être testé.

connection de la base à PhpStorm:
Dans le phpmyadmin, dans la base de mywishlist aller dans le menu “Privilèges
puis faire “ajouter un nouvel utilisateur” 
Remplir tous les champs ci-dessus à part les champs barrés en rouge. Dans le “Nom d’hôte”, choisir “local” puis après avoir rempli tous les champs, exécuter en bas de la page. Cet utilisateur va servir à accéder depuis phpstorm à notre base de données. Dans le phpstorm, appuyer sur 
 tout à droite
puis appuyer sur , sélectionner “data source” et choisir “MYSQL”. Par la suite, la page 
sera généré. Dans la ligne “user:” mettre le nom d’utilisateur précédemment utilisé lors de la création du nouvel utilisateur, puis dans le “Password” mettre le mot de passe précédemment utilisé. Dans le “Database:”, mettre dans la zone de texte “mywishlist” puis test connection pour pouvoir tester si cette base est correctement retrouvée. Après validation de celui-ci, appuyer sur “Apply” et “OK”. Normalement cette manipulation si elle marche correctement comme décrite sur chaque étape, la base de données sera par la suite accessible depuis phpstorm.

dans le dossier “conf” de “mywishlistapp_groupe6”, créer un fichier”db.conf.ini” et lui insérer les valeurs suivantes :
driver=mysql
host=localhost
database=mywishlist
username= (écrire votre nom d’utilisateur)
password=(écrire votre mot de passe)
charset=utf8
collation=utf8_unicode_ci

composer.json :
Pour installer le composer.json, aller vers le site https://getcomposer.org/  puis aller sur getting started < trouver la version compatible avec votre phpstorm (souvent la version pour windows). Après avoir téléchargé le composer, l’installer. Puis dans le terminal de phpstorm, se situer où le composer.json est situé puis faire un “composer install”. Votre Vendor et autoloader sera automatiquement généré.   

cliquez ici -> http://localhost/mywishlistapp_groupe6/
