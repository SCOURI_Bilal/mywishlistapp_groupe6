
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>MyWishList.app</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/styles.css">
  <script src="js/bootstrap.min.js" charset="utf-8"></script>
  <script src="js/jquery-3.5.1.min.js" charset="utf-8"></script>
</head>
<body class="bg-primary" data-spy="scroll" data-target=".navbar" data-offset="50">

  <script>
$(document).ready(function(){
    $(".navbar-toggler").click(function(){
        if ($("#navbarColor01").css("display") == "none") {
            $("#navbarColor01").css({"display": "inline"});
      }else {
            $("#navbarColor01").css({"display": "none"});
      }
    });
});
</script>

<nav class="navbar navbar-expand-lg navbar-dark bg-secondary sticky-top" style="height: 80px">
  <a class="navbar-brand" href="#">MyWishList.app</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse bg-secondary" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Acceuil
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#compte" role="button" aria-haspopup="true" aria-expanded="false">Compte</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="html/signup.html">Inscription</a>
          <a class="dropdown-item" href="html/login.html">Authentification</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#apropos">À propos</a>
      </li>
    </ul>
  </div>
</nav>

<div class="jumbotron bg-light">
  <h1 class="display-3">Bienvenue sur MyWishList.app</h1>
  <p class="lead">MyWishList.app est une application en ligne pour créer, partager et gérer des listes de cadeaux. </p>
  <hr class="my-4">
  <p>L'application permet à un utilisateur de créer une liste de souhaits à l'occasion d'un événement
    particulier (anniversaire, fin d'année, mariage, retraite …) et lui permet de diffuser cette liste de
    souhaits à un ensemble de personnes concernées. Ces personnes peuvent alors consulter cette liste
    et s'engager à offrir 1 élément de la liste. Cet élément est alors marqué comme réservé dans cette
    liste.</p>
    <p class="lead">
      <a class="btn btn-primary btn-lg" href="html/learnmore.html" role="button">En savoir plus</a>
    </p>
  </div>

  <div class="jumbotron bg-dark" id="compte">
    <h1 class="display-3 mt-5 text-white">Compte</h1>
    <div class="row">

      <div class="card border-info col-lg-5 mr-auto">
        <div class="card-header">S'inscrire</div>
        <div class="card-body">
          <h4 class="card-title">Inscription</h4>
          <p class="card-text" style="margin-top: 50px;">Vous n'avez toujours pas de compte ? <br> Aucun soucis, vous pouvez vous inscrire dès maintenant !</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="html/signup.html" role="button">Inscrivez-vous</a>
          </p>
        </div>
      </div>

      <div class="card border-success col-lg-5 ml-auto">
        <div class="card-header">Se connecter</div>
        <div class="card-body">
          <h4 class="card-title">Authentification</h4>
          <p class="card-text" style="margin-top: 50px">Vous avez déjà un compte ? <br> Alors connectez-vous pour avoir accès à votre liste. </p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="html/login.html" role="button">Connectez-vous</a>
          </p>
        </div>
      </div>

    </div>
  </div>

  <div class="container" id="apropos">
    <div class="jumbotron border-warning bg-light">
      <h1 class="display-3">À propos</h1>
      <p class="lead">Ce site a été créé dans le cadre du projet du module de Programmation Web / Serveur.</p>
      <hr class="my-4">
      <p>Les développeurs de ce site sont:<br>
         -Alex Mangenot<br>
         -Enes Kahraman<br>
         -Unser Kévin<br>
         -Alexandre Kobisch<br>
         -Bilal Scouri<br> <br>
         Étudiant en DUT informatique à l'IUT Nancy-Charlemagne dans le groupe S3B.
       </p>
    </div>
  </div>

</body>
</html>
