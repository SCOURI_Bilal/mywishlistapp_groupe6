<?php


namespace wish\view;


class CreateurView
{

    private $data;

    public function __construct(array $data){
        $this->data =$data;
    }

    private function uneListeHtml( array $val): string {

        $html = <<<END
            <section class="content">
            <a style="margin-right: 5px;"style="margin-right: 5px;" href="{$val[1]['containerDependance']->router->pathFor('affListeEdition',['tokenModification'=>$val[0]->tokenModification])}"> Editer </a>
            <form method="post" action="{$val[1]['containerDependance']->router->pathFor('listeSuppr',['tokenModification'=>$val[0]->tokenModification])}"><button type="submit"> Supprimer </button></form>
            <H3>Liste : {$val[0]->titre}</H3>
            <p>{$val[0]->description}</p>
            <h4>expire le : {$val[0]->expiration}</h4>
            <p>si tu veux partager ta liste avec tes amis donne leur cette url<br>
            {$val[1]['containerDependance']->router->pathFor('liste',['tokenPartage'=>$val[0]->tokenPartage])}</p>
            <p>------------------------------------------------------------------</p>
            </section>
            END;
        return $html;
    }

    public function render(array $vars){

        if(isset($this->data['errorMessage'])) {
            $errMessage = <<<END
<p class="errMessage" style="color: red;">
{$this->data['errorMessage']}
</p>
END;
        }
        else $errMessage = '';

        switch ($vars['renderfunc']) {
            case'affCreeListe':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <form method="post" action="{$vars['containerDependance']->router->pathFor('creationList')}">
                <div class="form-group">
                <label for="titreliste" class="text-info">Titre de la liste :*</label> <br>
                <input type="text" name="titreliste" id="titreliste" class="form-control">
                </div>
                <div class="form-group">
                <label for="descriptionliste" class="text-info">Description :</label> <br>
                <textarea type="text" name="descriptionliste" id="descriptionliste" class="form-control"></textarea>
                </div>
                <div class="form-group">
                <label for="dateexpirliste" class="text-info">Date d'expiration :</label> <br>
                <input type="date" name="dateexpirliste" id="dateexpirliste" class="form-control"> <br>
                {$errMessage}
                <button type="submit">Créer</button>
                </div>
                </form> <br>
                <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
                </body>
                END;

                break;
            case'creeListe':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p>La liste {$this->data['valTitre']} avec la description <br>
                {$this->data['valDescription']} <br> 
                et qui expire le {$this->data['valDateExpir']} <br>
                à été crée<br>
                l'url de partage (ne marche pas pour vous) est : {$vars['containerDependance']->router->pathFor('liste',['tokenPartage'=>$this->data['valtokenPartage']])}</p> <br>
                <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
                </body>
                END;

                break;
            case 'affAjouterItemListe':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <div>
                <form method="post" action="{$vars['containerDependance']->router->pathFor('ajoutItem',['tokenModification'=>$this->data[0]])}" >
                <div class="form-group">
                <label for="nomItem" class="text-info">nom de l'objet :*</label> <br>
                <input type="text" name="nomItem" id="nomItem" class="form-control">
                </div>
                <div class="form-group">
                <label for="descriptionItem" class="text-info">Description :</label> <br>
                <textarea type="text" name="descriptionItem" id="descriptionItem" class="form-control"></textarea>
                </div>
                <div class="form-group">
                <label for="urlItem" class="text-info">Url (qui mènerait au produit sur un site marchant par exemple) :</label> <br>
                <input type="URL" name="urlItem" id="urlItem" class="form-control">
                </div>
                <div class="form-group">
                <label for="prixItem" class="text-info">prix : </label> <br>
                <input type="number" min="0" step="0.01" name="prixItem" id="prixItem" class="form-control" > <br>
                {$errMessage}
                </div>
                <div class="form-group">
                <label for="urlImage" class="text-info"> Poster une url d'une image</label>
                <input type="URL" name="urlImage" id="urlImage" class="form-control"> <br>
                <button type="submit">Ajouter</button>
                </div>
                </form> <br>
                <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
                </body>
                END;
                break;

            case 'ajouterItemListe':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p>L'objet {$this->data['valNom']} avec la description <br>
                {$this->data['valDescription']} <br> 
                avec l'url <a href="{$this->data['valUrl']}">{$this->data['valUrl']}</a> <br>
                qui coût {$this->data['valPrix']} €<br>
                à été ajouter dans la liste nommé : {$this->data['valListeModif']->titre}
                <img src="{$this->data['valImage']}"/>
                </p> <br>
                <a href="{$vars['containerDependance']->router->pathFor('listeCreateur',['tokenModification'=>$this->data['valListeModif']->tokenModification])}">voir la liste</a> <br>
                <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
                </body>
                END;
                break;

            case 'displayListeCreateur':
                $affListe = $this->uneListeHtml([$this->data[0], $vars]);
                $affItem ='';
                foreach ($this->data[1] as list($itItem, $itReser)) {
                    if ($itReser != '0') {
                        $reser = <<<END
<H4 class="reserve">réservé</H4>
END;
                    } else {
                        $reser = <<<END
<H4 class="reserve">Non réservé</H4>
END;
                    }
                    $affItem = $affItem.<<<END
<section class="content">
                        <H3>{$itItem->nom}</H3>
                        <img src="{$itItem->img}"><br>
                        <a href="{$vars['containerDependance']->router->pathFor('itemCreat', ['id'=>$itItem->id])}">Afficher les détails</a>
                        $reser
                        <a style="margin-right: 5px;"style="margin-right: 5px;" href="{$vars['containerDependance']->router->pathFor('affItemEdition',['id'=>$itItem->id])}"> Editer </a>
                        <form method="post" action="{$vars['containerDependance']->router->pathFor('itemSuppr',['id'=>$itItem->id])}"><button type="submit"> Supprimer </button></form>
                        <p>.................................................................................................</p>
                        </section>

END;

                }



                $html = <<<END
        <!DOCTYPE html>
        <head>
        <link rel=""stylesheet" href="{$vars['basepath']}/wish.css"
        </head>
        <body>
        $affListe
        $affItem
        <a href='{$vars['containerDependance']->router->pathFor('affAjoutItem',['tokenModification'=>$this->data[0]->tokenModification])}'>ajouter un item</a> <br>
        <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
        </body>
END;
                break;

            case 'displayItemCrea':

                if ($this->data[1] != '0') {
                    $reser = <<<END
<H4 class="reserve">réservé</H4>
END;
                } else {
                    $reser = <<<END
<H4 class="reserve">Non réservé</H4>
END;
                }
                $affItem = <<<END
<section class="content">
                        <H3>{$this->data[0]->nom}</H3>
                        <img src="{$this->data[0]->img}"><br>
                        <p>Decription :{$this->data[0]->descr} </p>
                        <p>Tarif : {$this->data[0]->tarif}</p>
                        <a href="{$this->data[0]->url}">URL associé</a>
                        $reser
                        </section>

END;

                $html = <<<END
        <!DOCTYPE html>
        <head>
        <link rel=""stylesheet" href="{$vars['basepath']}/wish.css"
        </head>
        <body>
        $affItem
        <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
        </body>
END;
                break;

            case 'affEditionListe':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p>Ne modifie la valeur que des champs que tu veut modifier, <br>
                si tu veux en supprimer certains, il te suffit d'enlever leur valeur <br>
                attention les champs accompagné d'un * ne peut être null</p> <br>
                <form method="post" action="{$vars['containerDependance']->router->pathFor('listeEdition',['tokenModification'=>$this->data[0]->tokenModification])}">
                <div class="form-group">
                <label for="titreliste" class="text-info">Titre de la liste* :</label> <br>
                <input type="text" name="titreliste" id="titreliste" class="form-control" value="{$this->data[0]->titre}">
                </div>
                <div class="form-group">
                <label for="descriptionliste" class="text-info">Description :</label> <br>
                <input type="text" name="descriptionliste" id="descriptionliste" class="form-control" value="{$this->data[0]->description}">
                </div>
                <div class="form-group">
                <label for="dateexpirliste" class="text-info">Date d'expiration :</label> <br>
                <input type="date" name="dateexpirliste" id="dateexpirliste" class="form-control" value="{$this->data[0]->expiration}"> <br>
                {$errMessage}
                <button type="submit">editer</button>
                </div>
                </form> <br>
                <a href="{$vars['containerDependance']->router->pathFor('listeCreateur',['tokenModification'=>$this->data[0]->tokenModification])}">annuler</a>
                </body>
        END;

                break;

            case 'supprItemErr':

                $html = <<<END
<!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p> {$this->data[0]->nom} ne peut pas être supprimé car il est réservé</p>
                <a href="{$vars['containerDependance']->router->pathFor('listeCreateur',['tokenModification'=>$this->data[1]->tokenModification])}">revenir a la liste</a>
                </body>
END;
                break;

            case 'affEditionItem':

                $html = <<<END
                <!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p>Ne modifie la valeur que des champs que tu veut modifier, <br>
                si tu veux en supprimer certains, il te suffit d'enlever leur valeur <br>
                attention les champs accompagné d'un * ne peut être null</p> <br>
                <form method="post" action="{$vars['containerDependance']->router->pathFor('itemEdition',['id'=>$this->data[0]->id])}">
                <div class="form-group">
                <label for="nomItem" class="text-info">nom* :</label> <br>
                <input type="text" name="nomItem" id="nomItem" class="form-control" value="{$this->data[0]->nom}">
                </div>
                <div class="form-group">
                <label for="descriptionItem" class="text-info">Description :</label> <br>
                <input type="text" name="descriptionItem" id="descriptionItem" class="form-control" value="{$this->data[0]->descr}">
                </div>
                <div class="form-group">
                <label for="urlItem" class="text-info">url :</label> <br>
                <input type="url" name="urlItem" id="urlItem" class="form-control" value="{$this->data[0]->url}">
                </div>
                <div class="form-group">
                <label for="prixItem" class="text-info">tarif :</label> <br>
                <input type="number" min="0" step="0.01" name="prixItem" id="prixItem" class="form-control" value="{$this->data[0]->tarif}"> <br>
                {$errMessage}
                </div>             
                <div class="form-group">
                <label for="urlImage" class="text-info"> Changer url de l'image :</label>
                <input type="URL" name="urlImage" id="urlImage" class="form-control" value="{$this->data[0]->img}"> <br>
                <button type="submit">editer</button>
                </div>
                </form> <br>
                <a href="{$vars['containerDependance']->router->pathFor('listeCreateur',['tokenModification'=>$this->data[1]->tokenModification])}">annuler</a>
                </body>
        END;

                break;

            case 'editionItemErr':

                $html = <<<END
<!DOCTYPE html>
                <head>
                <link rel="stylesheet" href="{$vars['basepath']}/interface/quelquechose.css">
                </head>
                <body>
                <p> {$this->data[0]->nom} ne peut pas être modifié car il est réservé</p>
                <a href="{$vars['containerDependance']->router->pathFor('listeCreateur',['tokenModification'=>$this->data[1]->tokenModification])}">revenir a la liste</a>
                </body>
END;
                break;

            case'accesDenied':
                $html = <<<END
        <!DOCTYPE html>
        <head>
        <link rel=""stylesheet" href="{$vars['basepath']}/wish.css"
        </head>
        <body>
        <H1> access denied : {$vars['messErr']}</H1>
        <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a>
        </body>
END;
                break;
        }
        return $html;
    }
}