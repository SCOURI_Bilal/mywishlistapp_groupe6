<?php


namespace wish\view;


class ParticipantView
{


    private $data;

    public function __construct(array $data){
        $this->data =$data;
    }

    private function unItemHtml(array $val): string {

        $html = <<<END
            <section class="content">
            <H3>{$val[0][0]->nom}</H3>
            <p>{$val[0][0]->descr}</p>
            <h4>tarif : {$val[0][0]->tarif}</h4>
</section>
END;

        if(!empty($val[0][1])){
            $html = $html.<<<END
<H4 class="reserve">réservé par {$val[0][1]->nom_res}</H4>
END;
        }else{
            $html = $html.<<<END
            <form id="reserverItemForm" class="form" action="{$val[1]['containerDependance']->router->pathFor('reserverItem', ['id' => $val[0][0]->id])}" method="post">
                            <h3 class="text-center text-info">Reserver</h3>
                            <div class="form-group">
            END;
            if (!isset($val[0]['user'])) {
                $html = $html.<<<END
                                <label for="username_res" class="text-info">Saisir un nom d'utilisateur: </label><br>
                                <input type="text" name="username_res" id="username_res" class="form-control" required><br>                  
            END;
            }
            $html = $html.<<<END
                                <label for="message_res" class="text-body"> Saisir un message (facultatif) </label>
                                <input type="text" name="message_res" id="message_res" class="text-body">
                            </div>
                            <div class="form-group">
                                <label for="button" class="button"></label>
                                <input type="submit" name="reserverItem" class="btn btn-info btn-md text-white" value="Valider">
                            </div>
            </form>
END;

        }
        return $html;

    }

    private function uneListeHtml( \wish\models\Liste $liste): string {

        $html = <<<END
            <section class="content">
            <H3>Liste : {$liste->titre}</H3>
            <p>{$liste->description}</p>
            <h4>expire le : {$liste->expiration}</h4>
            <p>------------------------------------------------------------------</p>
            </section>
            END;
        return $html;

    }
    public function reserverItem(array $val): string
    {
            $html = <<<END
            <form id="reserverItemForm" class="form" action="{$val['c']->router->pathFor('reserverItem', ['id' => $val['data'][0]->id])}" method="post">
                            <h3 class="text-center text-info">Reserver</h3>
                            <div class="form-group">
            END;
        if (!isset($_SESSION['user'])) {
            $html = $html.<<<END
                                <label for="username_res" class="text-info">Saisir un nom d'utilisateur: </label><br>
                                <input type="text" name="username_res" id="username_res" class="form-control" required><br>                  
            END;
        }
        $html = $html.<<<END
                                <label for="message_res" class="text-body"> Saisir un message (facultatif) </label>
                                <input type="text" name="message_res" id="message_res" class="text-body">
                            </div>
                            <div class="form-group">
                                <label for="button" class="button"></label>
                                <input type="submit" name="reserverItem" class="btn btn-info btn-md text-white" value="Valider">
                            </div>
            </form>
END;
        return $html;
    }

    public function render(array $vars){
        if(isset($this->data['errorMessage'])) {
            $errMessage = <<<END
<p class="errMessage" style="color: red;">
{$this->data['errorMessage']}
</p>
END;
        }
        else $errMessage = '';
        switch ($vars[0]){
            case 'liste':
                $content = $this->uneListeHtml($this->data[0]);
                foreach ($this->data[1] as list($itItem, $itReser)){
                    if ($itReser != '0') {
                        $reser = <<<END
<H4 class="reserve">réservé par {$itReser->nom_res}</H4>
<p>{$itReser->message_res}</p>
END;
                    } else {
                        $reser = <<<END
<H4 class="reserve">Non réservé</H4>
END;
                    }
                    $details = <<<END
                        <section class="content">
                        <H3>{$itItem->nom}</H3>
                        <img src="{$itItem->img}"><br>
                        <a href="{$vars['containerDependance']->router->pathFor('item', ['id'=>$itItem->id])}">Afficher les détails</a>
                        $reser
                        <p>.................................................................................................</p>
                        </section>
                        END;
                     $content = $content . $details;
                }
                if(!empty($this->data[2])){
                    $message='';
                    foreach ($this->data[2] as $itmessage){
                        $message = $message.<<<END
<div><p>{$itmessage->message}</p></div>
END;

                    }
                    $content=$content.$message;
                }
                $content = $content.<<<END
<form method="post" action="{$vars['containerDependance']->router->pathFor('ajoutMessageListe',['tokenPartage'=>$this->data[0]->tokenPartage])}" >
        <div class="form-group">
        <label for="Message" class="text-info"> Tu peux poster un message ici : </label> <br>
        <textarea type="text" name="Message" id="Message" class="form-control"></textarea>
        {$errMessage}
        <button type="submit">Ajouter</button>
        </div>
        </form>
END;

                break;

            case 'item':
                $content = $this->unItemHtml([$this->data, $vars]).<<<END
<a href="{$vars['containerDependance']->router->pathFor('liste',['tokenPartage'=>$this->data[2]->tokenPartage])}">revenir a la liste</a><br>
END;
                break;
            default:
                $content = "";
        }

        $html = <<<END
        <!DOCTYPE html>
        <head>
        <link rel=""stylesheet" href="{$vars['basepath']}/wish.css"
        </head>
        <body> 
        $content
        </div>
        <a href="{$vars['containerDependance']->router->pathFor('index')}">acceuil</a> <br>
        </body>
        END;
        return $html;
    }
}