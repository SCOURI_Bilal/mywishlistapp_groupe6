<?php


namespace wish\view;


class CompteView
{
    private $data;

    public function __construct(array $data){
        $this->data =$data;
    }

    public function render(array $vars){

        if(isset($this->data['errorMessage'])) {
            $errMessage = <<<END
<p class="errMessage" style="color: red;">
{$this->data['errorMessage']}
</p>
END;
        }
        else $errMessage = '';

        switch ($vars['renderfunc']) {
            case 'affConnexion':

                $html = <<<END
                <!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>MyWishList.app-Login</title>
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/bootstrap.min.css">
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/style_login.css">
    <script src="{$vars['basepath']}/interface/js/jquery-3.5.1.min.js" charset="utf-8"></script>
</head>
<body class="bg-success">
<div id="login" >
    <h3 style="text-align : center;" class="pt-5"><a class="navbar-brand text-white" href="{$vars['containerDependance']->router->pathFor('index')}">MyWishList.app</a></h6>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="{$vars['containerDependance']->router->pathFor('connexion')}" method="post">
                            <h3 class="text-center text-success">Connexion</h3>
                            <div class="form-group">
                                <label for="username" class="text-success">Nom d'utilisateur:</label><br>
                                <input type="text" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-success">Mot de passe:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                                {$errMessage}
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Se connecter" class="btn btn-success btn-md text-white" value="Se connecter">
                            </div>
                            <div id="register-link" class="text-right mt-2">
                                <p>Vous n'avez toujours pas de compte ? Alors <a href="{$vars['containerDependance']->router->pathFor('affInscription')}" class="text-success">inscrivez-vous</a> !</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</html>

END;

                break;
            case 'affInscription':

                $html = <<<END
                <!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>MyWishList.app-Signup</title>
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/bootstrap.min.css">
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/style_login.css">
    <script src="{$vars['basepath']}/interface/js/jquery-3.5.1.min.js" charset="utf-8"></script>
</head>
<body class="bg-info">
<div id="login" >
    <h3 style="text-align : center;" class="pt-5"><a class="navbar-brand text-white" href="{$vars['containerDependance']->router->pathFor('index')}">MyWishList.app</a></h6>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                    
                        <form id="login-form" class="form" action="{$vars['containerDependance']->router->pathFor('inscription')}" method="post">
                            <h3 class="text-center text-info">Inscription</h3>
                            <div class="form-group">
                                <label for="username" class="text-info">Nom d'utilisateur:</label><br>
                                <input type="text" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Mot de passe:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                                {$errMessage}
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Se connecter" class="btn btn-info btn-md text-white" value="S'inscire">
                            </div>
                            <div id="register-link" class="text-right mt-2">
                                <p>Vous avez déjà un compte ? Alors <a href="{$vars['containerDependance']->router->pathFor('affConnexion')}" class="text-info">connectez-vous</a> !</p>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</html>
END;


                break;
            case 'inscription':

                $html = <<<END
                <!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>MyWishList.app-Signup</title>
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/bootstrap.min.css">
    <link rel="stylesheet" href="{$vars['basepath']}/interface/css/style_login.css">
    <script src="{$vars['basepath']}/interface/js/jquery-3.5.1.min.js" charset="utf-8"></script>
</head>
<body class="bg-info">
<div id="login" >
    <h6 style="text-align : center;" class="pt-5"><a style="font-size: 3.5rem;" class="navbar-brand text-white" href="{$vars['containerDependance']->router->pathFor('index')}">MyWishList.app</a></h6>  
</div>
<div style="text-align : center; margin-top: 20%" class="container">
      <h1>Bienvenu {$this->data['pseudo']} !</h1>
</div>
</body>

</html>
END;


                break;
        }

        return $html;
    }
}