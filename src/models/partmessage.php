<?php


namespace wish\models;


use Illuminate\Database\Eloquent\Model;

class partmessage extends Model
{
    protected $table = 'partmessage';
    protected $primaryKey = 'id';
    public $timestamps = false ;
}