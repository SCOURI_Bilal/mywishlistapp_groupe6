<?php


namespace wish\control;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use wish\models\Item;
use \wish\models\Liste;
use \wish\view\IndexView;

class IndexController
{
    private $c = null; //container de dépendance

    function __construct(\Slim\Container $c){
        $this->c = $c;
    }

    function affIndex(Request $rq, Response $rs, array $args): Response {

        session_start();

        $data = [];
        $data['mesListes'] = [];

        $htmlvars =[
            'basepath'=> $rq->getUri()->getBasePath(),
            'renderfunc'=>'affIndex',
            'containerDependance'=> $this->c];

        if(isset($_SESSION['user']))
        {
            $data['user'] = $_SESSION['user'];
            try{
                $data['mesListes'] = Liste::query()
                    ->where('user_id','=',$_SESSION['user']['id'])
                    ->get();
            }catch(\Exception $e){echo $e;}
        }
        else
        {
            if(isset($_COOKIE['anonymousInfoCrea']))
            {
                $cook = json_decode($_COOKIE['anonymousInfoCrea']);
                foreach ($cook as $itIdListe)
                {
                    array_push(
                        $data['mesListes'],
                        Liste::query()
                            ->where('no', '=', $itIdListe)
                            ->first()
                    );
                }
            }


        }

        $v = new IndexView($data);

        $rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }

    /*
    en élaboration
    function visua(Request $rq, Response $rs, array $args)
    {
        try {
            $liste = Liste::select('no', 'titre', 'description', 'expiration');
        } catch (ModelNotFoundException $e) {
            $rs->getBody()->write("liste non trouvé");
            return $rs;
        }
        echo $this->$liste('no');
    }
*/
}