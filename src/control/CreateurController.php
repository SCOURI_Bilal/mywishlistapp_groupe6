<?php

namespace wish\control;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use wish\models\Item;
use \wish\models\Liste;
use wish\models\Reservation;
use \wish\view\CreateurView;

class CreateurController
{
    private $c = null; //container de dépendance

    function __construct(\Slim\Container $c){
        $this->c = $c;
    }

    function affCreeList (Request $rq, Response $rs, array $args): Response
    {
        $htmlvars =[
            'basepath'=> $rq->getUri()->getBasePath(),
            'renderfunc'=>'affCreeListe',
            'containerDependance'=> $this->c
        ];

        $v = new CreateurView([1]);

        $rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }

    public function creeList(Request $rq, Response $rs, array $args): Response
    {
        session_start();

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        $titre = $rq->getParsedBody()['titreliste'];
        $description = $rq->getParsedBody()['descriptionliste'];
        $dateExpiration = $rq->getParsedBody()['dateexpirliste'];

        if(!empty($titre))
        {
            try {
                $nouvliste = new Liste();

                $id_user = null;
                if (isset($_SESSION['user'])) {
                    $id_user = $_SESSION['user']['id'];
                    $anonyme = 'faux';
                } else {
                    $anonyme = 'vrai';
                }
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $description = filter_var($description, FILTER_SANITIZE_SPECIAL_CHARS);

                $nouvliste->titre = $titre;
                $nouvliste->user_id = $id_user;
                $nouvliste->description = $description;
                $nouvliste->expiration = $dateExpiration;
                $tokenPart = bin2hex(openssl_random_pseudo_bytes(32));
                $nouvliste->tokenPartage = $tokenPart;
                $tokenModif = bin2hex(openssl_random_pseudo_bytes(32));
                $nouvliste->tokenModification = $tokenModif;
                $nouvliste->save();

                if($anonyme == 'vrai') {
                    if (isset($_COOKIE['anonymousInfoCrea'])) {
                        $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                        array_push($temp, $nouvliste->no);
                        setcookie('anonymousInfoCrea',json_encode($temp), time() + 60 * 60 * 24 * 365,'/');
                    } else {
                        setcookie('anonymousInfoCrea',json_encode([$nouvliste->no]), time() + 60 * 60 * 24 * 365,'/');
                    }
                }
            }catch (\Exception $e)
            {echo $e;}

            $data = [
                'valTitre' => $titre,
                'valDescription' => $description,
                'valDateExpir' => $dateExpiration,
                'valtokenPartage' => $tokenPart
            ];
            $htmlvars['renderfunc'] = 'creeListe';

        }
        else
        {
            $htmlvars['renderfunc'] = 'affCreeListe';
            $data['errorMessage'] = 'veuillez au moins renseigner le Titre';
        }
        $v = new CreateurView($data);

        $rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }

    public function affAjouterItemList(Request $rq, Response $rs, array $args):Response
    {
        session_start();

        $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        $tokenModif = $args['tokenModification'];
        try {
            $liste = Liste::query()->where('tokenModification', '=', $tokenModif)->firstOrFail();

            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }

            if (($userid == $liste->user_id) || ($idCook == $liste->no)) {

                $htmlvars['renderfunc'] = 'affAjouterItemListe';

                $v = new CreateurView([$tokenModif]);

                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            } else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;

            }
        }catch (\Exception $e){$rs->getBody()->write($e);return $rs;}
    }

    public function ajouterItemList(Request $rq, Response $rs, array $args):Response
    {
        session_start();

        $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        $tokenModif = $args['tokenModification'];
        try {
            $liste = Liste::query()->where('tokenModification', '=', $tokenModif)->firstOrFail();

            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }

            if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
                $nom = $rq->getParsedBody()['nomItem'];
                $description = $rq->getParsedBody()['descriptionItem'];
                $url = $rq->getParsedBody()['urlItem'];
                $prix = $rq->getParsedBody()['prixItem'];
                $img_des = $rq->getParsedBody()['urlImage'];


                if (!empty($nom)) {

                    $nom = filter_var($nom, FILTER_SANITIZE_STRING);
                    $description = filter_var($description, FILTER_SANITIZE_SPECIAL_CHARS);
                    $url = filter_var($url, FILTER_SANITIZE_URL);
                    $img_des = filter_var($img_des, FILTER_SANITIZE_URL);

                    $ListModif = $liste;

                    $ItemAj = new Item();
                    $ItemAj->liste_id = $ListModif->no;
                    $ItemAj->nom = $nom;
                    $ItemAj->descr = $description;
                    $ItemAj->img = $img_des;
                    $ItemAj->url = $url;
                    $ItemAj->tarif = $prix;
                    $ItemAj->save();

                    $data = [
                        'valNom' => $nom,
                        'valDescription' => $description,
                        'valImage' => $img_des,
                        'valUrl' => $url,
                        'valPrix' => $prix,
                        'valListeModif' => $ListModif
                    ];
                    $htmlvars['renderfunc'] = 'ajouterItemListe';
                } else {
                    $htmlvars['renderfunc'] = 'affAjouterItemListe';
                    $data['errorMessage'] = 'veuillez au moins enseigner le nom';
                }
                $v = new CreateurView($data);

                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;

            }


        } catch (\Exception $e) {$rs->getBody()->write($e);return $rs;}
    }

    public function displayListeCreateur(Request $rq, Response $rs, array $args):Response
    {
        session_start();
        $htmlvars =['basepath'=> $rq->getUri()->getBasePath(),
            'containerDependance'=> $this->c];

        $tokenModif = $args['tokenModification'];

        try{

            $liste = Liste::query()->where('tokenModification', '=', $tokenModif)->firstOrFail();

            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }

            if (($userid == $liste->user_id) || ($idCook == $liste->no)) {

                $item = Item::query()->where('liste_id', '=', $liste->no)
                    ->get();

                $itemAndReser = [];
                foreach ($item as $itItem) {
                    if (!is_null(Reservation::query()->where('id_item', '=', $itItem->id)->first()))
                        $reservation = Reservation::query()->where('id_item', '=', $itItem->id)->first();
                    else $reservation = '0';
                    array_push($itemAndReser, [$itItem, $reservation]);
                }

                $htmlvars['renderfunc'] = 'displayListeCreateur';

                $v = new CreateurView([$liste, $itemAndReser]);

                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;

            }
        }catch (ModelNotFoundException $e){$rs->getBody()->write($e);return $rs;}

    }

    public function displayItemCrea (Request $rq, Response $rs, array $args): Response{
        session_start();

        $item_id = $args['id'];
        try{
            $item = Item::query()->where('id', '=', $item_id)
                ->firstOrFail();
        }
        catch (ModelNotFoundException $e){
            $rs->getBody()->write("item {$item_id} non trouvé");
            return $rs;
        }
        $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        $liste = Liste::query()->where('no', '=', $item->liste_id)->firstOrFail();

        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }


        if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
            if (!is_null(Reservation::query()->where('id_item', '=', $item_id)->first())) {
                $reservation = Reservation::query()
                    ->where('id_item', '=', $item_id)
                    ->first();
            } else {
                $reservation = '0';
            }

            $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
                'containerDependance' => $this->c, 'item',
                'renderfunc' => 'displayItemCrea'];

            $data = [$item, $reservation];

            $v = new CreateurView($data);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;

        }
    }

    public function affEditionListe (Request $rq, Response $rs, array $args): Response{
        session_start();
        $htmlvars =['basepath'=> $rq->getUri()->getBasePath(),
            'renderfunc'=>'affEditionListe',
            'containerDependance'=> $this->c];

        $tokenModif = $args['tokenModification'];

        try {
            $liste = Liste::query()
                ->where('tokenModification', '=', $tokenModif)
                ->first();
        }catch (\Exception $e){$rs->getBody()->write($e);return $rs;}
        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
            $v = new CreateurView([$liste]);

            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;

        }
    }

    public function editionListe (Request $rq, Response $rs, array $args): Response{
        session_start();
        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        $titre = $rq->getParsedBody()['titreliste'];
        $description = $rq->getParsedBody()['descriptionliste'];
        $dateExpiration = $rq->getParsedBody()['dateexpirliste'];

        try {
            $liste = Liste::query()
                ->where('tokenModification','=',$args['tokenModification'])
                ->firstOrFail();

            $titre = filter_var($titre, FILTER_SANITIZE_STRING);
            $description = filter_var($description, FILTER_SANITIZE_SPECIAL_CHARS);

            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }

            if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
                if (!empty($titre)) {
                    $liste->titre = $titre;

                    $liste->description = $description;
                    $liste->expiration = $dateExpiration;
                    $liste->save();
                } else {
                    $htmlvars['renderfunc'] = 'affEditionListe';
                    $data = [$liste];
                    $data['errorMessage'] = 'le Titre est obligatoire';
                    $v = new CreateurView($data);
                    $rs->getBody()->write($v->render($htmlvars));
                    return $rs;
                }
            }else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;

            }

        }catch (\Exception $e)
        {$rs->getBody()->write($e);return $rs;}

        $rs = $rs->withRedirect($this->c->router->pathFor('listeCreateur',['tokenModification'=>$args['tokenModification']]));
        return $rs;


    }

    public function supprimerListe (Request $rq, Response $rs, array $args): Response{

        session_start();
        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];
        try {
            $liste = Liste::query()
                ->where('tokenModification','=',$args['tokenModification'])
                ->firstOrFail();
            $item = Item::query()
                ->where('liste_id','=',$liste->no)
                ->get();
        }catch (\Exception $e){$rs->getBody()->write($e);return $rs;}

        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
            foreach ($item as $itItem) {
                $itItem->delete();
            }
            if (!isset($_SESSION['user'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                unset($temp[array_search($liste->no, $temp)]);
                setcookie('anonymousInfoCrea', json_encode($temp), time() + 60 * 60 * 24 * 365, '/');
            }
            $liste->delete();

            return $rs->withRedirect($this->c->router->pathFor('index'));
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;

        }
    }

    public function affEditionItem (Request $rq, Response $rs, array $args): Response{
        session_start();
        $htmlvars =['basepath'=> $rq->getUri()->getBasePath(),
            'containerDependance'=> $this->c];

        $id = $args['id'];

        try {
            $item = Item::query()
                ->where('id', '=', $id)
                ->first();
            $liste = Liste::query()
                ->where('no','=',$item->liste_id)
                ->firstOrFail();
            $reservation = Reservation::query()
                ->where('id_item','=',$item->id)
                ->first();

        }catch (\Exception $e){$rs->getBody()->write($e);return $rs;}


        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
            if (is_null($reservation)) {
                $v = new CreateurView([$item, $liste]);
                $htmlvars['renderfunc']='affEditionItem';
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            } else {
                $htmlvars['renderfunc'] = 'editionItemErr';
                $v = new CreateurView([$item, $liste]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;

        }
    }

    public function editionItem (Request $rq, Response $rs, array $args): Response
    {
        session_start();
        $nom = $rq->getParsedBody()['nomItem'];
        $description = $rq->getParsedBody()['descriptionItem'];
        $url = $rq->getParsedBody()['urlItem'];
        $prix = $rq->getParsedBody()['prixItem'];
        $imageUrl = $rq->getParsedBody()['urlImage'];
        $htmlvars =[
            'basepath'=> $rq->getUri()->getBasePath(),
            'containerDependance'=> $this->c];



        $nom = filter_var($nom, FILTER_SANITIZE_STRING);
        $description = filter_var($description, FILTER_SANITIZE_SPECIAL_CHARS);
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $imageUrl = filter_var($imageUrl, FILTER_SANITIZE_URL);
        try {
            $item = Item::query()
                ->where('id', '=', $args['id'])
                ->firstOrFail();
            $liste = Liste::query()
                ->where('no','=',$item->liste_id)
                ->firstOrFail();


            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }

            if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
                if (!empty($nom)) {

                    $item->nom = $nom;
                    $item->descr = $description;
                    $item->url = $url;
                    $item->tarif = $prix;
                    $item->img = $imageUrl;
                    $item->save();

                    $rs = $rs->withRedirect($this->c->router->pathFor('listeCreateur', ['tokenModification' => $liste->tokenModification]));
                    return $rs;
                } else {
                    $htmlvars['renderfunc'] = 'affEditionItem';
                    $data = [$item, $liste];
                    $data['errorMessage'] = 'le nom est obligatoire';
                    $v = new CreateurView($data);

                    $rs->getBody()->write($v->render($htmlvars));
                    return $rs;
                }
            }else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }
        }
        catch (\Exception $e)
        {$rs->getBody()->write($e);return $rs;}
    }

    public function supprimerItem (Request $rq, Response $rs, array $args): Response{

        session_start();

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        try {
            $Item = Item::query()
                ->where('id','=',$args['id'])
                ->firstOrFail();
            $liste = Liste::query()
                ->where('no','=',$Item->liste_id)
                ->firstOrFail();
            $reservation = Reservation::query()
                ->where('id_item','=',$Item->id)
                ->first();
        }catch (\Exception $e){$rs->getBody()->write($e);return $rs;}


        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid == $liste->user_id) || ($idCook == $liste->no)) {
            if (is_null($reservation)) {
                $Item->delete();
                return $rs->withRedirect($this->c->router->pathFor('listeCreateur', ['tokenModification' => $liste->tokenModification]));
            } else {
                $htmlvars['renderfunc'] = 'supprItemErr';
                $v = new CreateurView([$Item, $liste]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas le créateur de {$liste->titre}";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;

        }


    }

}