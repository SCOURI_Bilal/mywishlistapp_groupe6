<?php

namespace wish\control;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \wish\models\Item;
use \wish\models\Liste;
use wish\models\partmessage;
use \wish\models\Reservation;
use wish\view\CreateurView;
use \wish\view\ParticipantView;

class ParticipantController{

    private $c = null; //container de dépendance

    function __construct(\Slim\Container $c){
        $this->c = $c;
    }

    function displayItem (Request $rq, Response $rs, array $args): Response{
        session_start();
        $item_id = $args['id'];
        $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];

        try{
            $item = Item::query()->where('id', '=', $item_id)
                ->firstOrFail();
            $liste = Liste::query()->where('no','=',$item->liste_id)
                ->firstOrFail();
        }
        catch (ModelNotFoundException $e){
            $rs->getBody()->write("item {$item_id} non trouvé");
            return $rs;
        }


        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid != $liste->user_id) && ($idCook != $liste->no)) {

            $reservation = Reservation::query()
                ->where('id_item', '=', $item_id)
                ->first();

            array_push($htmlvars, 'item');
            $data = [$item, $reservation, $liste];
            if (isset($_SESSION['user']))
                $data['user'] = $_SESSION['user'];

            $v = new ParticipantView($data);

            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = " En tant que créateur, vous ne pouvez pas voir {$liste->titre} en tant que participant.";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }
    }

    function reserverItem(Request $rq, Response $rs, array $args): Response{
        session_start();
        $id_item = $args['id'];
        $htmlvars = ['basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c];
        try{
            $item = Item::query()->where('id', '=', $id_item)
                ->firstOrFail();
            $liste = Liste::query()->where('no', '=', $item->liste_id)
                ->firstOrFail();

        }catch (ModelNotFoundException $e) {
            $rs->getBody()->write("item $id_item non trouvé");
            return $rs;
        }
        $idCook = 'no';
        if (isset($_COOKIE['anonymousInfoCrea'])) {
            $temp = json_decode($_COOKIE['anonymousInfoCrea']);
            $idCook = $temp[array_search($liste->no, $temp)];
        }
        $userid = 'no';
        if (isset($_SESSION['user'])) {
            $idCook = 'no';
            $userid = $_SESSION['user']['id'];
        }

        if (($userid != $liste->user_id) && ($idCook != $liste->no)) {
            if(isset($_SESSION['user'])){
                $nom = $_SESSION['user']['pseudo'];
            }else{
                $nom = htmlspecialchars($rq->getParsedBody()['username_res']);
            }
            if (!empty($nom)) {

                if (!empty($rq->getParsedBody()['message_res']))
                    $message = htmlspecialchars($rq->getParsedBody()['message_res']);
                else
                    $message = '';

                Reservation::query()->insert(['id_item' => $id_item, 'nom_res' => $nom, 'message_res' => $message]);
            }


            $rs = $rs->withRedirect($this->c->router->pathFor('liste',['tokenPartage'=>$liste->tokenPartage]));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = " En tant que créateur, vous ne pouvez pas voir {$liste->titre} en tant que participant.";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }
    }
    function ajouterMessageListe (Request $rq, Response $rs, array $args): Response
    {
        session_start();
        try{
            $liste = Liste::query()->where('tokenPartage', '=', $args['tokenPartage'])
                ->firstOrFail();
            $liMessage = partmessage::query()->where('no','=',$liste->no)
                ->get();
            $item = Item::query()->where('liste_id', '=', $liste->no)
                ->get();
            $itemAndReser = [];
            foreach ($item as $itItem) {
                if (!is_null(Reservation::query()->where('id_item', '=', $itItem->id)->first()))
                    $reservation = Reservation::query()->where('id_item', '=', $itItem->id)->first();
                else $reservation = '0';
                array_push($itemAndReser, [$itItem, $reservation]);
            }
            $htmlvars =['basepath'=> $rq->getUri()->getBasePath(), 'containerDependance'=> $this->c];
            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }
            $message = $rq->getParsedBody()['Message'];
            $message = filter_var($message, FILTER_SANITIZE_SPECIAL_CHARS);
            if (($userid != $liste->user_id) && ($idCook != $liste->no)) {

            if(!empty($message)){
                $partmessage = new partmessage();
                $partmessage->no = $liste->no;
                $partmessage->message = $message;
                $partmessage->save();
                $rs = $rs->withRedirect($this->c->router->pathFor('liste',['tokenPartage'=>$liste->tokenPartage]));
                return $rs;
                        }else{
                array_push($htmlvars, 'liste');
                $data = [$liste, $itemAndReser, $liMessage];
                $data['errorMessage'] = 'on ne peut ajouter un message vide';
                $v = new ParticipantView($data);

                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
                }
            }  else {
        $htmlvars['renderfunc'] = 'accesDenied';
        $htmlvars['messErr'] = " En tant que créateur, vous ne pouvez pas voir {$liste->titre} en tant que participant.";

        $v = new CreateurView([1]);
        $rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }
        } catch (ModelNotFoundException $e){
            $rs->getBody()->write("liste {$liste->no} non trouvé");
            return $rs;
        }
    }

    function displayListe (Request $rq, Response $rs, array $args): Response{
        session_start();
        try{
            $liste = Liste::query()->where('tokenPartage', '=', $args['tokenPartage'])
                ->firstOrFail();
            $message = partmessage::query()->where('no','=',$liste->no)
                ->get();
            $item = Item::query()->where('liste_id', '=', $liste->no)
                ->get();
            $itemAndReser = [];
            foreach ($item as $itItem) {
                if (!is_null(Reservation::query()->where('id_item', '=', $itItem->id)->first()))
                    $reservation = Reservation::query()->where('id_item', '=', $itItem->id)->first();
                else $reservation = '0';
                array_push($itemAndReser, [$itItem, $reservation]);
            }

            $htmlvars =['basepath'=> $rq->getUri()->getBasePath(), 'containerDependance'=> $this->c];

            $idCook = 'no';
            if (isset($_COOKIE['anonymousInfoCrea'])) {
                $temp = json_decode($_COOKIE['anonymousInfoCrea']);
                $idCook = $temp[array_search($liste->no, $temp)];
            }
            $userid = 'no';
            if (isset($_SESSION['user'])) {
                $idCook = 'no';
                $userid = $_SESSION['user']['id'];
            }
            if (($userid != $liste->user_id) && ($idCook != $liste->no)) {
                array_push($htmlvars,'liste');
                $v = new ParticipantView([$liste, $itemAndReser,$message]);


                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }else {
                $htmlvars['renderfunc'] = 'accesDenied';
                $htmlvars['messErr'] = " En tant que créateur, vous ne pouvez pas voir {$liste->titre} en tant que participant.";

                $v = new CreateurView([1]);
                $rs->getBody()->write($v->render($htmlvars));
                return $rs;
            }
        }catch (ModelNotFoundException $e){
            $rs->getBody()->write("liste {$liste->no} non trouvé");
            return $rs;
        }
    }
}