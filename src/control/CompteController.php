<?php


namespace wish\control;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use wish\models\Item;
use \wish\models\Liste;
use \wish\models\Compte;
use \wish\view\CompteView;
use wish\view\CreateurView;
use wish\view\IndexView;

class CompteController
{
    private $c = null; //container de dépendance

    function __construct(\Slim\Container $c){
        $this->c = $c;
    }

    function affConnexion (Request $rq, Response $rs, array $args): Response
    {
        session_start();

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c
        ];

        $user = 'no';
        if (isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
        }

        if ($user == 'no') {

            $htmlvars['renderfunc'] = 'affConnexion';

            $v = new CompteView([1]);

            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous êtes déjà connecté";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }
    }

    function affInscription (Request $rq, Response $rs, array $args): Response
    {session_start();

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c
        ];

        $user = 'no';
        if (isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
        }

        if ($user == 'no') {
            $htmlvars['renderfunc'] = 'affInscription';
            $v = new CompteView([1]);

            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous ne pouvez pas vous inscrire si vous êtes connecté";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }
    }

    function connexion (Request $rq, Response $rs, array $args): Response
    {
        session_start();
        $data = [];

        $htmlvars =[
            'basepath'=> $rq->getUri()->getBasePath(),
            'containerDependance'=> $this->c
        ];
        try {

            if (!empty($rq->getParsedBody()['username']) && !empty($rq->getParsedBody()['password'])) {

                $pseudo = $rq->getParsedBody()['username'];
                $mdp = $rq->getParsedBody()['password'];
                $pseudo = filter_var($pseudo,FILTER_SANITIZE_STRING);

                $listPseudo = Compte::query()->where('pseudo','=',$pseudo)
                    ->get();

                $profile = null;
                foreach ($listPseudo as $pseudoVerife) {
                    $hash = $pseudoVerife->pass;
                    if (password_verify($mdp, $hash)) {
                        $profile = $pseudoVerife;
                    }
                }
                if(!is_null($profile))
                {
                    $_SESSION['user'] = ['id' => $profile->id, 'pseudo' => $profile->pseudo];
                    $htmlvars['renderfunc'] = 'affIndex';
                    $rs = $rs->withRedirect($this->c->router->pathFor('index'));
                }
                else
                {
                    $htmlvars['renderfunc'] = 'affConnexion';
                    $data['errorMessage'] = 'ce couple login/password est invalide';
                    $v = new CompteView($data);
                }

            }
            else
            {
                $htmlvars['renderfunc'] = 'affConnexion';
                $data['errorMessage'] = 'veillez à ce que les 2 champs soient remplit';
                $v = new CompteView($data);
            }
        }
        catch (\Exception $e){
            echo $e;
            $data['errorMessage'] = $e;
            $v = new CompteView($data);
        }
        if(isset($v))$rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }

    function inscription (Request $rq, Response $rs, array $args): Response
    {
        session_start();
        $data = [];

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c
        ];

        if (!empty($rq->getParsedBody()['username']) && !empty($rq->getParsedBody()['password']))
        {
            $pseudo = $rq->getParsedBody()['username'];
            $mdp = $rq->getParsedBody()['password'];

            $pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);
            $hash = password_hash($mdp, PASSWORD_DEFAULT);

            if (password_verify($mdp, $hash))
            {
                $nouvCompte = new Compte();
                $nouvCompte->pseudo = $pseudo;
                $nouvCompte->pass = $hash;
                $nouvCompte->save();

                $_SESSION['user'] = ['id' => $nouvCompte->id, 'pseudo' => $nouvCompte->pseudo];

                $htmlvars['renderfunc'] = 'inscription';
                $data['pseudo'] = $pseudo;
            }
            else
            {
                $htmlvars['renderfunc'] = 'affInscription';
                $data['errorMessage'] = 'password error';
            }
        }
        else
        {
            $htmlvars['renderfunc'] = 'affInscription';
            $data['errorMessage'] = 'veillez a ce que les 2 champs soient remplis';
        }
        $v = new CompteView($data);

        $rs->getBody()->write($v->render($htmlvars));
        return $rs;
    }

    function deconnexion (Request $rq, Response $rs, array $args): Response
    {
        session_start();

        $htmlvars = [
            'basepath' => $rq->getUri()->getBasePath(),
            'containerDependance' => $this->c
        ];

        $user = 'no';
        if (isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
        }
        if ($user != 'no') {
            session_start();
            session_destroy();
            $rs = $rs->withRedirect($this->c->router->pathFor('index'));
            return $rs;
        }else {
            $htmlvars['renderfunc'] = 'accesDenied';
            $htmlvars['messErr'] = "vous n'êtes pas connecté";

            $v = new CreateurView([1]);
            $rs->getBody()->write($v->render($htmlvars));
            return $rs;
        }
    }

}